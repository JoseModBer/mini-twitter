-- Creamos y usamos la database minitwitter
DROP DATABASE IF EXISTS minitwitter;
CREATE DATABASE IF NOT EXISTS minitwitter DEFAULT CHARACTER SET utf8;
USE minitwitter;

-- Ordenamos los drop en este orden, en función de las relacciones de tablas.
DROP TABLE IF EXISTS tweets; -- contiene referencias a users.
DROP TABLE IF EXISTS users;

-- creamos la tabla users y sus campos.
    CREATE TABLE users (
      id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
      email VARCHAR(100) UNIQUE NOT NULL,
      password VARCHAR(100) NOT NULL,
      createdAt TIMESTAMP NOT NULL
    );

-- creamos la tabla tweets y sus campos.
    CREATE TABLE tweets (
      id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
      idUser INT UNSIGNED NOT NULL,
      FOREIGN KEY (idUser) REFERENCES users(id),
      text VARCHAR(280) NOT NULL,
      image VARCHAR(100),
      createdAt TIMESTAMP NOT NULL
    );