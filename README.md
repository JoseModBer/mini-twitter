# Simple Twitter API

Este ejercicio consiste en crear una API que simule el funcionamiento de una aplicación similar a Twitter.

## Instalar

- Crear una base de datos vacía en una instancia de MySQL local.

  - Abrir con workbench el archivo documentation/DDL-minitwitter.sql y ejecutar las siguientes líneas para crear la DB:
    DROP DATABASE IF EXISTS minitwitter;
    CREATE DATABASE IF NOT EXISTS minitwitter DEFAULT CHARACTER SET utf8;
    USE minitwitter;

- Guardar el archivo `.env.example` como `.env` y cubrir los datos necesarios.

- Ejecutar `npm run initDB` para crear las tablas necesarias en la base de datos anteriormente creada.

- Ejecutar `npm run dev` o `npm start` para lanzar el servidor.

#################################################################################################################################

## Entidades

- Users:

  - id
  - email
  - password
  - createdAt
  - modifiedAt

- Tweets:
  - id
  - idUser
  - text
  - image (opcional)
  - createdAt
  - modifiedAt

#################################################################################################################################

## Endpoints

Usuarios:

- 1. POST [/users] - Registro de usuario. ✅
- 2.  POST [/login] - Login de usuario (devuelve token). ✅
- 3.  GET [/users] - Devuelve información del usuario que figure en el token. **TOKEN** ✅

Tweets:

- 4.  POST [/tweets] - Permite crear un tweet. **TOKEN** ✅
- 5.  GET [/tweets] - Lista todos los tweets. ✅
- 6.  GET [/tweets/:idTweet] - Devuelve info de un tweet concreto. ✅
- 7.  DELETE [/tweets/:id] - Borra un tweet solo si eres quien lo creó. **TOKEN** ✅

#################################################################################################################################

# NOTA SOBRE TOKENS EN POSTMAN

- Tras crear un usuario y hacer el login del usuario, se nos dará un TOKEN para ese usuario.
- La opción básica es copiar ese token e ir copiándolo como valor de "Authorization" en las peticiones que requieran token.
- La opción Cool es copiar ese token y seguir estos pasos:
  - En el nombre de la colección de postman, pinchamos en los tres puntitos de la derecha y le damos a "edit".
  - Se nos abre una pestaña con el nobre de la colección. Vamos a donde pone "variables".
  - Donde pone "add new variable" escribimos "token_user_1" para el usuario 1. Donde pone "current value" pegamos el TOKEN.
  - Le damos a guardar.
  - Ahora cada vez que se nos pida TOKEN en una petición, en el campo "Authorization" meteremos como valor "{{token_user_1}}"

var jsonData = pm.response.json();
pm.globals.set("TOKEN_jose", jsonData.data.token);
