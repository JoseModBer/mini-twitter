// ############################################# PASOS INICIALES TERMINAL ###########################

/**########################################################
 * ## CREAR ENTORNO NODE + INSTALAR DEPENDENCIAS/MÓDULOS ##
 * ########################################################
 */
// CREAR ENTORNO DE NODE: creamos package.json con:
//      npm init -y

// NOTA: LAS DEPENDENCIAS DE DESARROLLADOR NO SE INSTALAN EN LA VERSIÓN FINAL DE LA API.

// instalamos mysql2 y dotenv (Ya se crea la carpeta node_modules)
//      npm i mysql2 dotenv

// instalamos eslint como dependencia de Desarrollador | avisa de errores en el código.
//      npm i -D eslint
// Configuramos eslint copiando el .eslintrc.json o con:
//      npx eslint --init | david le añade una rule al fichero.

// instalamos express y morgan
//      npm i express morgan

// instalamos nodemon
//      npm i nodemon -D

// instalamos bcryp para encriptar las contaseñas: (en vez de usar el ssh2 que usa stefano)
//      npm i bcrypt

// instalamos jwt para generar tokens
//      npm i jsonwebtoken

// Instalamos fileupload | sirve para poder deserializar un body en formato "form-data" (peticiones con archivos o imágenes) y poder crear req.files
//      npm i express-fileupload

// Instalamos sharp para gestionar las fotos:
//      npm i sharp

// Instalamos uuid | para crear un nombre aleatorio para la imágen
//      npm i uuid

// Instalamos cors | evita problemas con las CORS a la hora de conectar nuestar PI con REACT (u otros clientes).
//      npm i cors

// ############################################# PASOS INICIALES DB ##################################

/**#######################################################
 * ## CREAR LA DB VACÍA EN UNA INSTACNIA DE MySQL LOCAL ##
 * #######################################################
 */
// Abrir con workbench el archivo documentation/DDL-minitwitter.sql y ejecutar las siguientes líneas para crear la DB:
//      DROP DATABASE IF EXISTS minitwitter;
//      CREATE DATABASE IF NOT EXISTS minitwitter DEFAULT CHARACTER SET utf8;
//      USE minitwitter;

/**########################################################
 * ## CREAR GETCONNECTION.JS Y INITDB.JS ##
 * ########################################################
 */
// Creamos getConnection.js (FUNCIÓN getDB) y luego initDB.js (FUNCIÓN createDatabase).
// Lanzamos la creación de las tablas en la DB con:
//      node bbdd/initDB.js

// ############################################### REQUERIMOS COSAS ########################################
`use strict`;
// Esto necesita ir en la linea antes de hacer el destructuring de las variables de entorno del .env PERO se suele poner arriba del todo.
require('dotenv').config();
// Requerimos express, fileupload, morgan y cors:
const express = require('express');
const fileUpload = require('express-fileupload');
const morgan = require('morgan');
const cors = require('cors');

// ############################################### PASOS INICIALES ########################################

// Creamos el servidor.
const app = express();

// PASO NECESARIO PARA REACT | Middleware que evita problemas con las CORS a la hora de conectar nuestra API con React (u otros clientes).
app.use(cors());

// PASO NECESARIO PARA REACT | Middleware que indica donde se encuentran los archivos estáticos de mi servidor.
app.use(express.static(process.env.UPLOADS_DIR));

// Middleware que deserializa un body en formato "raw" creando la propiedad "body" en el objeto "request".
app.use(express.json());

// Middleware que deserializa un body en formato "form-data" creando la propiedad "files" en el objeto "request" (PRECISA fileUpload).
app.use(fileUpload());

// Middleware que muestra por consola información sobre la petición entrante.
app.use(morgan('dev'));

// ############################################### CONTROLADOR CASUAL (carpeta middlewares) ################

/**
 * ###########################################
 * ## Controladores casuales (middlewares/) ##
 * ###########################################
 */

const isAuth = require('./middlewares/isAuth');

// ############################################# PETICIONES - MIDDLEWARE ENDPOINTS #########################

/**
 * ####################################
 * ## Middlewares-EndPoints Usuarios ##
 * ####################################
 */
// Importamos los controladores de las peticiones de usuarios:
const { newUser, loginUser, getOwnUser } = require('./controllers/users');

// 1. Crear un nuevo usuario | POST [/users] - Registro de usuario.
app.post('/users', newUser);
// newUser requiere la FUNCIÓN QUERY insertUserQuery.js de (bbdd/queries/users)

// 2. Login de usuario | POST [/login] - (devuelve token).
app.post('/users/login', loginUser);
// loginUser requiere la FUNCIÓN QUERY selectUserByEmailQuery.js de (bbdd/queries/users)

// 3. Obtener información del usuario del token | GET [/users] - (ESTA PETICIÓN REQUIERE TOKEN).
app.get('/users', isAuth, getOwnUser);
// getOwnUser requiere la FUNCIÓN isAuth.js de (middlewares) y la FUNCIÓN QUERY selectUserByIdQuery.js de (bbdd/queries/users)

/**
 * ##################################
 * ## Middlewares-EndPoints Tweets ##
 * ##################################
 */
// Importamos los controladores de las peticiones de tweets:
const {
  newTweet,
  listTweets,
  getTweet,
  deleteTweet,
} = require('./controllers/tweets');

// 4. Crear un nuevo tweet | POST [/tweets] - (ESTA PETICIÓN REQUIERE TOKEN).
app.post('/tweets', isAuth, newTweet);
// newTweet requiere la FUNCIÓN isAuth.js de (middlewares) y la FUNCIÓN QUERY insertTweetQuery.js de (bbdd/queries/tweets)

// 5. Obtenemos el listado de tweets | GET [/tweets]
app.get('/tweets', listTweets);
// listTweets requiere la FUNCIÓN QUERY selectAllTweetsQuery.js de (bbdd/queries/tweets)

// 6. Obtener info de un tweet en concreto | GET [/tweets/:idTweet]
app.get('/tweets/:idTweet', getTweet);
// getTweet requiere la FUNCIÓN QUERY selectTweetByIdQuery.js de (bbdd/queries/tweets)

// 7. Eliminar un tweet | DELETE [/tweets/:id] - (ESTA PETICIÓN REQUIERE TOKEN).
app.delete('/tweets/:idTweet', isAuth, deleteTweet);
// deleteTweet requiere la FUNCIÓN isAuth.js de (middlewares) y las FUNCIÓNES QUERY selectTweetByIdQuery.js y deleteTweetQuery.js de (bbdd/queries/tweets)

// ############################################# MIDDLEWARES - ERRORES #############################

/**########################################
 * ### MIDDLEWARE - ERRORES CONTROLADOS ###
 * ########################################
 */
// Middleware de error.
app.use((err, req, res, next) => {
  console.error(err);

  res.status(err.statusCode || 500).send({
    status: 'error',
    message: err.message,
  });
});

/**##############################
 * ### MIDDLEWARE - ERROR 404 ###
 * ##############################
 */
// Middleware de ruta no encontrada.
app.use((req, res) => {
  res.status(404).send({
    status: 'error',
    message: 'Ruta no encontrada',
  });
});

// ############################################### ACTIVACIÓN SERVIDOR ##############################

/**#########################################################
 * ######### MIDDLEWARE - ACTIVACIÓN SERVIDOR ##############
 * #########################################################
 */
// Pongo en escucha express (servidor funcionando en un puerto pendiente de escuchar peticiones): (esto siempre al final)
app.listen(process.env.PORT, () => {
  console.log(`Server listening at http://localhost:${process.env.PORT}`);
});
