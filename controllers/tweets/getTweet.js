// ############################################### REQUERIMOS COSAS ########################################

const selectTweetByIdQuery = require('../../bbdd/queries/tweets/selectTweetByIdQuery');

// ############################################### FUNCIÓN CONTROLADORA getTweet ############################

const getTweet = async (req, res, next) => {
  try {
    const { idTweet } = req.params;

    // Info del tweet.
    // Llamamos a la FUNCIÓN QUERY selectTweetByIdQuery de (bbdd/queries/tweets)
    const tweet = await selectTweetByIdQuery(idTweet);

    // Respuesta de la petición:
    res.send({
      status: 'ok',
      data: {
        tweet,
      },
    });
  } catch (err) {
    next(err); // mandamos error al middleware de errroes de server.js
  }
};
// Exportamos la FUNCIÓN CONTROLADORA getTweet (para poder importarla en (controllers/tweets/index.js) y de ahí exportarla para importarla en server.js)
module.exports = getTweet;
