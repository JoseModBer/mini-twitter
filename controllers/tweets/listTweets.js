// ############################################### REQUERIMOS COSAS ########################################

const selectAllTweetsQuery = require('../../bbdd/queries/tweets/selectAllTweetsQuery');

// ############################################### FUNCIÓN CONTROLADORA listTweets ############################

const listTweets = async (req, res, next) => {
  try {
    const { keyword } = req.query;

    // Lista de tweets.
    // Llamamos a la FUNCIÓN QUERY selectAllTweetsQuery de (bbdd/queries/tweets)
    const tweets = await selectAllTweetsQuery(keyword);

    // Respuesta de la petición:
    res.send({
      status: 'ok',
      data: {
        tweets,
      },
    });
  } catch (err) {
    next(err); // mandamos error al middleware de errroes de server.js
  }
};

// Exportamos la FUNCIÓN CONTROLADORA listTweets (para poder importarla en (controllers/tweets/index.js) y de ahí exportarla para importarla en server.js)
module.exports = listTweets;
