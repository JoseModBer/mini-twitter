// ############################################### REQUERIMOS COSAS ########################################

const selectTweetByIdQuery = require('../../bbdd/queries/tweets/selectTweetByIdQuery');
const deleteTweetQuery = require('../../bbdd/queries/tweets/deleteTweetQuery');

const { generateError, deletePhoto } = require('../../helpers');

// ############################################### FUNCIÓN CONTROLADORA deleteTweet ############################

const deleteTweet = async (req, res, next) => {
  try {
    const { idTweet } = req.params;

    // Obtenemos la info del tweet.
    // Llamamos a la FUNCIÓN QUERY selectTweetByIdQuery de (bbdd/queries/tweets)
    const tweet = await selectTweetByIdQuery(idTweet);

    // Si no soy el propietario del tweet lanzamos un error.
    if (req.user.id !== tweet.idUser) {
      generateError('No tienes suficientes permisos', 401);
    }

    // Si el tweet tiene una imagen vinculada la borramos de la carpeta "uploas".
    if (tweet.image) {
      await deletePhoto(tweet.image);
    }

    // Borramos el tweet.
    // Llamamos a la FUNCIÓN deleteTweetQuery de (bbdd/queries/tweets)
    await deleteTweetQuery(idTweet);

    // Respuesta de la petición:
    res.send({
      status: 'ok',
      message: 'Tweet eliminado',
    });
  } catch (err) {
    next(err); // mandamos error al middleware de errroes de server.js
  }
};

// Exportamos la FUNCIÓN CONTROLADORA deleteTweet (para poder importarla en (controllers/tweets/index.js) y de ahí exportarla para importarla en server.js)
module.exports = deleteTweet;
