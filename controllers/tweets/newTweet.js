// ############################################### REQUERIMOS COSAS ########################################

const insertTweetQuery = require('../../bbdd/queries/tweets/insertTweetQuery');

const { generateError, savePhoto } = require('../../helpers');

// ############################################### FUNCIÓN CONTROLADORA newTweet ############################

const newTweet = async (req, res, next) => {
  try {
    const { text } = req.body;

    if (!text) {
      generateError('Faltan campos', 400);
    }

    // Variable donde almacenaremos el nombre de la imagen (si existe);
    let imgName;

    // Comprobamos si existe una imagen.
    if (req.files?.image) {
      // Guardamos la imagen en la carpeta "uploads" y obtenemos el nombre de la misma.
      imgName = await savePhoto(req.files.image);
    }

    // Insertamos el nuevo tweet.
    // Llamamos a la FUNCIÓN QUERY insertTweetQuery de (bbdd/queries/tweets)
    await insertTweetQuery(text, imgName, req.user.id);

    // Respuesta de la petición:
    res.send({
      status: 'ok',
      message: 'Tweet creado',
    });
  } catch (err) {
    next(err); // mandamos error al middleware de errroes de server.js
  }
};

// Exportamos la FUNCIÓN CONTROLADORA newTweet (para poder importarla en (controllers/tweets/index.js) y de ahí exportarla para importarla en server.js)
module.exports = newTweet;
