// ############################################### REQUERIMOS COSAS ########################################

const selectUserByIdQuery = require('../../bbdd/queries/users/selectUserByIdQuery');

// ############################################### FUNCIÓN CONTROLADORA getOwnUser ############################

const getOwnUser = async (req, res, next) => {
  try {
    // Obtenemos la información del usuario.
    // Llamamos a la FUNCIÓN QUERY selectUserByIdQuery de (bbdd/queries/users)
    const user = await selectUserByIdQuery(req.user.id);

    // Respuesta de la petición:
    res.send({
      status: 'ok',
      data: {
        user,
      },
    });
  } catch (err) {
    next(err); // mandamos error al middleware de errroes de server.js
  }
};

// Exportamos la FUNCIÓN CONTROLADORA getOwnUser (para poder importarla en (controllers/users/index.js) y de ahí exportarla para importarla en server.js)
module.exports = getOwnUser;
