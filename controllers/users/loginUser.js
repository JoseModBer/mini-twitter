// ############################################### REQUERIMOS COSAS ########################################

const selectUserByEmailQuery = require('../../bbdd/queries/users/selectUserByEmailQuery');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { generateError } = require('../../helpers');

// ############################################### FUNCIÓN CONTROLADORA loginUser ############################

const loginUser = async (req, res, next) => {
  try {
    // Obtenemos los datos (campos) del body de la petición.
    const { email, password } = req.body;

    // Si faltan campos lanzamos un error.
    if (!email || !password) {
      generateError('Faltan campos', 400);
    }

    // Localizamos al usuario con el email del body.
    // Llamamos a la FUNCIÓN QUERY selectUserByEmailQuery de (bbdd/queries/users)
    const user = await selectUserByEmailQuery(email);

    // Comprobamos si las contraseñas coinciden. Nos pide dos contraseñas password que es la encriptada y user.password que es la no encriptada. Devuelve un booleano
    const validPass = await bcrypt.compare(password, user.password);

    // Si la contraseña es incorrecta lanzamos un error.
    if (!validPass) {
      generateError('Contraseña incorrecta', 401);
    }

    // Objeto con información que queremos agregar al token.
    const tokenInfo = {
      id: user.id,
    };

    // Creamos el token (lo definimos en .env. Aporreamos el teclado para crearlo):
    const token = jwt.sign(tokenInfo, process.env.SECRET, {
      expiresIn: '7d',
    });

    // Respuesta de la petición:
    res.send({
      status: 'ok',
      data: {
        token,
      },
    });
  } catch (err) {
    next(err); // mandamos error al middleware de errroes de server.js
  }
};

// Exportamos la FUNCIÓN CONTROLADORA loginUser (para poder importarla en (controllers/users/index.js) y de ahí exportarla para importarla en server.js)
module.exports = loginUser;
