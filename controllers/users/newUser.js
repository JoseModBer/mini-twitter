// ############################################### REQUERIMOS COSAS ########################################

const insertUserQuery = require('../../bbdd/queries/users/insertUserQuery');
const { generateError } = require('../../helpers');

// ############################################### FUNCIÓN CONTROLADORA newUser ############################

const newUser = async (req, res, next) => {
  try {
    // Obtenemos los campos del body de la petición.
    const { email, password } = req.body;

    // Si faltan campos lanzamos un error.
    if (!email || !password) {
      generateError('Faltan campos', 400);
    }

    // Insertamos el usuario en la DB.
    // Llamamos a la FUNCIÓN QUERY insertUserQuery de (bbdd/queries/users)
    await insertUserQuery(email, password);

    // Respuesta de la petición:
    res.send({
      status: 'ok',
      message: 'Usuario creado',
    });
  } catch (err) {
    next(err); // mandamos error al middleware de errroes de server.js
  }
};

// Exportamos la FUNCIÓN CONTROLADORA newUser (para poder importarla en (controllers/users/index.js) y de ahí exportarla para importarla en server.js)
module.exports = newUser;
