// ############################################### REQUERIMOS COSAS ########################################
const jwt = require('jsonwebtoken');

const { generateError } = require('../helpers');

// ############################################### FUNCIÓN isAuth ########################################
// Función que usaremos recurrentemente para verificar si un usuario está logueado. La usaremos antes de lanzar los middleware-EndPoints de peticiones que exijan TOKEN.

const isAuth = async (req, res, next) => {
  try {
    // Aunque en Postman debe ir con mayúscula, aquí authorization debe ir con minúscula.
    // El token vendrá en la propiedad "authorization" de los headers.
    const { authorization } = req.headers;

    if (!authorization) {
      generateError('Falta la cabecera de autenticación', 400);
    }

    // Variable que almacenará la información del token.
    let tokenInfo;

    // Esto lo hacemos para que el error que se lance sea en español y no en inglés (jwt está en inglés)
    try {
      // Intentamos obtener la info del token.
      tokenInfo = jwt.verify(authorization, process.env.SECRET);
    } catch {
      generateError('Token incorrecto', 401);
    }

    // Creamos una nueva propiedad (inventada por nosotros) en el objeto "request" para guardar la información del token.
    req.user = tokenInfo;

    // Pasamos el control a la siguiente función controladora.
    next();
  } catch (err) {
    next(err);
  }
};

// ############################################### EXPORTAMOS FUNCIÓN isAuth ########################################
module.exports = isAuth;
