// ############################################### REQUERIMOS COSAS ########################################
const fs = require('fs/promises');
const path = require('path');
const sharp = require('sharp');
const { v4: uuid } = require('uuid');

// ############################################### FUNCIÓN generateError ##################################

/**
 * ####################
 * ## Generate Error ##
 * ####################
 */

const generateError = (msg, code) => {
  const err = new Error(msg);
  err.statusCode = code;
  throw err;
};

// ############################################### FUNCIÓN savePhoto ##################################

/**
 * ################
 * ## Save Photo ##
 * ################
 */

const savePhoto = async (img) => {
  // Creamos la ruta absoluta al directoriod e subida de imágenes.
  const uploadsPath = path.join(__dirname, process.env.UPLOADS_DIR);

  try {
    // Comprobamos si existe el directorio de subida de imágenes.
    await fs.access(uploadsPath);
  } catch {
    // Si el directorio no existe el método "access" lanza un error por
    // lo que entramos en el catch. Creamos el directorio.
    await fs.mkdir(uploadsPath);
  }

  // Procesamos la imagen y la convertimos en un objeto de tipo Sharp.
  const sharpImg = sharp(img.data);

  // Redimensionamos la iamgen a 500px de ancho para evitar que ocupe mucho espacio.
  sharpImg.resize(500);

  // Generamos un nombre único para la imagen.
  const imgName = `${uuid()}.jpg`;

  // Generamos la ruta absoluta a la imagen.
  const imgPath = path.join(uploadsPath, imgName);

  // Guardamos la imagen en el directorio de "uploads".
  await sharpImg.toFile(imgPath);

  // Retornamos el nombre con el que hemos guardado la imagen.
  return imgName;
};

// ############################################### FUNCIÓN deletePhoto ##################################

/**
 * ##################
 * ## Delete Photo ##
 * ##################
 */

const deletePhoto = async (photoName) => {
  // Creamos la ruta absoluta a la foto.
  const photoPath = path.join(__dirname, process.env.UPLOADS_DIR, photoName);

  try {
    // Comprobamos si existe la imagen.
    await fs.access(photoPath);
  } catch {
    // Si no existe finalizamos la función.
    return;
  }

  // Borramos la foto de la carpeta "uploads".
  await fs.unlink(photoPath);
};

// ############################################### EXPORTAMOS LAS FUNCIONES HELPERS ######################

module.exports = {
  generateError,
  savePhoto,
  deletePhoto,
};
