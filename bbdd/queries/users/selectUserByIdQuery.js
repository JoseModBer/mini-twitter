// ############################################### REQUERIMOS COSAS ########################################

const getDB = require('../../getConnection');

const { generateError } = require('../../../helpers');

// ############################################### FUNCIÓN QUERY selectUserByIdQuery #############

const selectUserByIdQuery = async (idUser) => {
  let connection;

  try {
    connection = await getDB();

    const [users] = await connection.query(
      `SELECT id, email, createdAt FROM users WHERE id = ?`,
      [idUser]
    );

    // Si el array de usuarios está vacío lanzo un error.
    if (users.length < 1) {
      generateError('Usuario no encontrado', 404);
    }

    // Retornamos al usuario de la posición 0.
    return users[0];
  } finally {
    if (connection) connection.release();
  }
};

// Exportamos la FUNCIÓN QUERY selectUserByIdQuery para poder importarla en la FUNCIÓN CONTROLADORA getOwnUser.js
module.exports = selectUserByIdQuery;
