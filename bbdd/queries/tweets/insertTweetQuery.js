// ############################################### REQUERIMOS COSAS ########################################

const getDB = require('../../getConnection');

// ############################################### FUNCIÓN QUERY insertTweetQuery #############

const insertTweetQuery = async (text, image, idUser) => {
  let connection;

  try {
    connection = await getDB();

    await connection.query(
      `
                INSERT INTO tweets (text, image, idUser, createdAt)
                VALUES (?, ?, ?, ?)
            `,
      [text, image, idUser, new Date()]
    );
  } finally {
    if (connection) connection.release();
  }
};

// Exportamos la FUNCIÓN QUERY insertTweetQuery para poder importarla en la FUNCIÓN CONTROLADORA newTweet.js
module.exports = insertTweetQuery;
