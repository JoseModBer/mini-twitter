// ############################################### REQUERIMOS COSAS ########################################

const getDB = require('../../getConnection');

// ############################################### FUNCIÓN QUERY deleteTweetQuery #############

const deleteTweetQuery = async (idTweet) => {
  let connection;

  try {
    connection = await getDB();

    await connection.query(`DELETE FROM tweets WHERE id = ?`, [idTweet]);
  } finally {
    if (connection) connection.release();
  }
};

// Exportamos la FUNCIÓN QUERY selectAllTweetsQuery para poder importarla en la FUNCIÓN CONTROLADORA deleteTweet.js
module.exports = deleteTweetQuery;
