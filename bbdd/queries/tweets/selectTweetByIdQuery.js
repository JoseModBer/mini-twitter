// ############################################### REQUERIMOS COSAS ########################################

const getDB = require('../../getConnection');

const { generateError } = require('../../../helpers');

// ############################################### FUNCIÓN QUERY selectTweetByIdQuery #############

const selectTweetByIdQuery = async (idTweet) => {
  let connection;

  try {
    connection = await getDB();

    const [tweets] = await connection.query(
      `SELECT * FROM tweets WHERE id = ?`,
      [idTweet]
    );

    if (tweets.length < 1) {
      generateError('Tweet no encontrado', 404);
    }

    return tweets[0];
  } finally {
    if (connection) connection.release();
  }
};

// Exportamos la FUNCIÓN QUERY selectAllTweetsQuery para poder importarla en la FUNCIÓN CONTROLADORA listTweets.js y deleteTweet.js
module.exports = selectTweetByIdQuery;
