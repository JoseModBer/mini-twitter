// ############################################### REQUERIMOS COSAS ########################################

const getDB = require('../../getConnection');

// ############################################### FUNCIÓN QUERY selectAllTweetsQuery #############

const selectAllTweetsQuery = async (keyword = '') => {
  let connection;

  try {
    connection = await getDB();

    const [tweets] = await connection.query(
      `SELECT * FROM tweets WHERE text LIKE ? ORDER BY createdAt DESC`,
      [`%${keyword}%`]
    );

    return tweets;
  } finally {
    if (connection) connection.release();
  }
};

// Exportamos la FUNCIÓN QUERY selectAllTweetsQuery para poder importarla en la FUNCIÓN CONTROLADORA listTweets.js
module.exports = selectAllTweetsQuery;
