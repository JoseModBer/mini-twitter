// ############################################### COMANDO CREACIÓN TABLAS DB #######################

// La database la crearemos y la elegiremos con workbench:
//      DROP DATABASE IF EXISTS minitwitter;
//      CREATE DATABASE IF NOT EXISTS minitwitter DEFAULT CHARACTER SET utf8;
//      USE minitwitter;
// Lanzamos la creación de las tablas en la DB con:
//      node bbdd/initDB.js

// ############################################### initDB.js ########################################

// Esto necesita ir en la linea antes de hacer el destructuring de las variables de entorno del .env PERO se suele poner arriba del todo.
require('dotenv').config();

// Requerimos la FUNCIÓN getDB (getConnection.js):
const getDB = require('./getConnection');

// ############################################### FUNCIÓN createDatabase ###########################

// Como estoy en mysql2.promises tiene que ser asíncrona
async function createDatabase() {
  // Variable que almacenará una conexión libre con la base de datos.
  let connection;

  try {
    // Intentamos obtener una conexión libre con la DB.
    connection = await getDB();

    console.log('Borrando tablas existentes...');

    await connection.query('DROP TABLE IF EXISTS tweets');
    await connection.query('DROP TABLE IF EXISTS users');

    console.log('Creando tablas...');

    await connection.query(`
            CREATE TABLE users (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                email VARCHAR(100) UNIQUE NOT NULL,
                password VARCHAR(100) NOT NULL,
                createdAt TIMESTAMP NOT NULL
            )
        `);

    await connection.query(`
            CREATE TABLE tweets (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                idUser INT UNSIGNED NOT NULL,
                FOREIGN KEY (idUser) REFERENCES users(id),
                text VARCHAR(280) NOT NULL,
                image VARCHAR(100),
                createdAt TIMESTAMP NOT NULL
            )
        `);

    console.log('¡Tablas creadas!');
  } catch (err) {
    console.error(err);
  } finally {
    // Si hay conexión, la liberamos.
    if (connection) connection.release();

    // Cerramos el proceso.
    process.exit();
  }
}

// Llamamos a la función anterior.
createDatabase();
