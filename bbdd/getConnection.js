// Requerimos mysql2 | OJO: Aquí es promise en singular. En fs, por ejemplo, es en plural.
const mysql = require('mysql2/promise');

// Importamos, del .env, las variables de entorno que necesitamos:
const { MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_BBDD } = process.env;
// console.log(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_BBDD); // Comprobamos que las tenemos con node bbdd/getConnection.js

// ############################################### FUNCIÓN getDB ########################################

// Variable que almacenará un grupo de conexiones.
let pool;

// Función que retorna una conexión libre con la base de datos.
const getDB = async () => {
  try {
    // Si no existe un grupo de conexiones lo creamos.
    if (!pool) {
      pool = mysql.createPool({
        connectionLimit: 10,
        host: MYSQL_HOST,
        user: MYSQL_USER,
        password: MYSQL_PASS,
        database: MYSQL_BBDD,
        timezone: 'Z', // Misma zona horaria para todos los países (hora zulú)
      });
    }

    // Retornamos una conexión libre con la base de datos.
    return await pool.getConnection();
  } catch (err) {
    console.error(err);

    throw new Error('Error al conectar con MySQL');
  }
};

// Exportamos la FUNCIÓN getDB:
module.exports = getDB;
